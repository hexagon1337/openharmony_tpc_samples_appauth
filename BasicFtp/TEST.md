## FTP单元测试用例

该测试用例基于OpenHarmony系统下，进行单元测试

### 单元测试用例覆盖情况

|接口名 | 是否通过 |备注|
|---|---|---|
|list|pass||
|uplodFrom|pass||
|uploadFromDir|pass||
|downloadTo|pass||
|downloadDir|pass||
|size|pass||
|features|pass||
|lastMod|pass||
|rename|pass||
|cdup|pass||
|ensureDir|pass||
|removeEmptyDir|pass||
|remove|pass||
|removeDir|pass||
|clearWorkingDir|pass||
|cd|pass||