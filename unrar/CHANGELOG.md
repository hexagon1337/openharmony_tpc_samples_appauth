## V2.0.2

1.支持x86编译

## V2.0.2-rc.0

1.修复不兼容API9问题

## V2.0.1

1.适配DevEco Studio: 4.0 (4.0.3.512), SDK: API10 (4.0.10.9)

2.ArkTS语法整改


## V2.0.0

1.适配DevEco Studio: 4.0 Canary1(4.0.0.112), SDK: API10 (4.0.7.2)


## v1.0.0

基于[Unrar](https://www.rarlab.com/rar_add.htm) 原库进行适配，使其可以运行在 OpenHarmony，并沿用其现有用法和特性。已支持：
- 判断压缩包名是否加密
- 解压文件到指定目标路径
- 解压无加密/加密压缩包