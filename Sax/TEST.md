# Sax单元测试用例

该测试用例基于OpenHarmony系统下，进行单元测试

单元测试用例覆盖情况

|              接口名               | 是否通过	 | 备注  |
|:------------------------------:|:-----:|:---:|
|       parser(a: boolean)       | pass  |     |
|   onopentag(node: ESObject)    | pass  |     |
| onclosetag(nodeName: ESObject) | pass  |     |
|     ontext(text: ESObject)     | pass  |     |
|    onend(ondeEnd: ESObject)    | pass  |     |
|    onerror(error: ESObject)    | pass  |     |
|    oncdata(error: ESObject)    | pass  |     |
|     oncomment(a: ESObject)     | pass  |     |
|  opentagstart(text: ESObject)  | pass  |     |
|    onattribute(a: ESObject)    | pass  |     |
|       write(str: String)       | pass  |     |
|             end()              | pass  |     |
|            close()             | pass  |     |