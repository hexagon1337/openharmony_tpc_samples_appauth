/**
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 *
 * This software is distributed under a license. The full license
 * agreement can be found in the file LICENSE in this distribution.
 * This software may not be copied, modified, sold or distributed
 * other than expressed in the named license agreement.
 *
 * This software is distributed without any warranty.
 */

export enum PresenceType {
    Chat = "0", /** 空闲 */
    Available = "1", /**在线 */
    Away = "2", /**离开 */
    Xa = "3", /**长时间离开 */
    Dnd = "4", /**勿扰 */
}
