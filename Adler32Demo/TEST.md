## js-adler32单元测试用例

该测试用例基于OpenHarmony系统下，参照[原库测试用例](https://github.com/SheetJS/js-adler32/blob/master/test.js) 进行单元测试

### 单元测试用例覆盖情况

|接口名 | 是否通过 |备注|
|---|---|---|
|buf(data:number[] or Unit8Array, seed?:number)|pass|
|bstr(data:string,seed?:number)|pass|
|str(data:string, seed?:number)|pass|
