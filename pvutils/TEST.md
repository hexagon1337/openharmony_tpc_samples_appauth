## pvutils单元测试用例

该测试用例基于OpenHarmony系统环境进行单元测试

### 单元测试用例覆盖情况

| 接口名                 | 是否通过 |备注|
|---------------------|---|---|
| utilConcatView      |pass|
| bufferToHexCodes    |pass|
| arrayBufferToString |pass|
| getUTCDate          |pass|
| getParametersValue  |pass|
| checkBufferParams   |pass|
| utilFromBase        |pass|
| utilToBase          |pass|
| padNumber           |pass|
| utilEncodeTC        |pass|
| toBase64            |pass|
| fromBase64          |pass|
| stringToArrayBuffer |pass|
| nearestPowerOf2     |pass|
| clearProps          |pass|
| isEqualBuffer       |pass|
| utilConcatBuf       |pass|

