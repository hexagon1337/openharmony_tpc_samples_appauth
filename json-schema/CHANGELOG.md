## v2.0.0
- 本库是基于jsonschema源库v1.4.1版本代码进行OpenHarmony环境接口适配验证的三方库。

- 包管理工具由npm切换为ohpm

- 适配DevEco Studio:  3.1 Beta2(3.1.0.400)

  适配SDK: API9 Release(3.2.11.9) 

- 适配DevEco Studio:  4.0 Canary1(4.0.3.212)

  适配SDK: API 10 Release(4.0.8.3)

## v1.0.0

发布适配OpenHarmony的sample demo

