/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export const model1 = {
  id: 0,
  firstName: "Umed Khudoiberdiev",
  caption: { text: "cool photo" },
  photos: [
    {
      id: 1,
      filename: "me.jpg",
      users: [],
      caption: { text: "cool photo" },
    },
    {
      id: 2,
      filename: "she.jpg",
      users: [],
      caption: { text: "cool photo" },
    },
  ],
};

export const model2 = {
  id: 0,
  firstName: "Umed",
  lastName: "Khudoiberdiev",
  password: "imnosuperman",
};

export const model3 = {
  firstName: "Umed",
  lastName: "Khudoiberdiev",
  password: "",
};

export const model42 = { id: 0, firstName: "Umed", lastName: "Khudoiberdiev" };

export const model4 = { implicitTypeNumber: "100", implicitTypeString: 133123 };

export const model5 = { implicitTypeNumber: 100, implicitTypeString: "133123" };

export const model6 = { "100000000": "100000000" };

export const model7 = {
  firstName: undefined,
  lastName: undefined,
};

export const model8 = {};

export const model9 = [
  {
    id: 1,
    age: 27,
    firstName: "Umed",
    lastName: "Khudoiberdiev",
    name: "Umed Khudoiberdiev",
  },
  {
    id: 2,
    age: 30,
    firstName: "Dima",
    lastName: "Zotov",
    name: "Dima Zotov",
  },
];

export const model10 = {
  id: 1,
  age: 27,
  firstName: "Umed",
  lastName: "Khudoiberdiev",
  password: "imnosuperman",
};

export const model11 = {
  firstName: "Umed",
  lastName: "Khudoiberdiev",
  age: 12,
};

export const model12 = {
  id: 1,
  firstName: "Umed",
  lastName: "Khudoiberdiev",
  password: "imnosuperman",
};

export const model13 = { valueOne: 42, valueTwo: 42 };

export const model14 = {
  id: 1,
  age: 27,
  firstName: "Umed",
  lastName: "Khudoiberdiev",
  password: "yayayaya",
};

export const model15 = { id: 1, age: 27, password: "yayayaya" };

export const model16 = {
  id: 1,
  firstName: "Umed",
  lastName: "Khudoiberdiev",
};

export const model17 = [
  {
    firstName: "Umed",
    lastName: "Khudoiberdiev",
    name: "Umed Khudoiberdiev",
  },
  {
    firstName: "Dima",
    lastName: "Zotov",
    name: "Dima Zotov",
  },
];

export const model18 = {
  name: "Umed Khudoiberdiev",
  photo: {
    id: 1,
    status: 1,
  },
};

export const model19 = {
  myName: "Umed",
  secondName: "Khudoiberdiev",
  name: "Umed Khudoiberdiev",
  fullName: "Umed Khudoiberdiev",
};

export const model20 = {
  lastName: "Khudoiberdiev",
  photo: {
    id: 1,
    status: 1,
  },
  photos: [
    {
      id: 1,
      status: 1,
    },
  ],
};

export const model21 = {
  id: 1,
  age: 27,
};

export const model22 = {
  id: 1,
};

export const model23 = {
  id: 1,
  age: 27,
  firstName: "Umed",
  lastName: "Khudoiberdiev",
};

export const model24 = {
  firstName: "Umed",
};

export const model25 = {
  id: 1,
  age: 27,
  firstName: "Umed",
};

export const model26 = {
  id: 1,
  firstName: "Umed",
};

export const model27 = {
  id: 1,
  age: 27,
  photo: { id: 2, description: "photo" },
};

export const model28 = {
  firstName: "Umed",
  lastName: "Khudoiberdiev",
  photo: {
    id: 1,
    filename: "iam.jpg",
  },
};

export const model29 = {
  firstName: "Umed",
  photo: {
    id: 1,
  },
};

export const model30 = {
  firstName: "Umed",
  lastName: "Khudoiberdiev",
  password: "imnosuperman",
  photo: {
    id: 1,
    filename: "myphoto.jpg",
  },
};

export const model31 = {
  id: 1,
  firstName: "Umed",
  lastName: "Khudoiberdiev",
  password: "imnosuperman",
  photo: {
    id: 1,
    metadata: "taken by Camera",
    filename: "myphoto.jpg",
  },
};

export const model32 = {
  firstName: "Umed",
  isActive: false,
  photo: {
    id: 1,
    status: 1,
  },
  photos: [
    {
      id: 1,
      status: 1,
    },
  ],
};

export const model33 = {
  firstName: "Umed",
  lastName: "Khudoiberdiev",
  password: "imnosuperman",
  isActive: false,
  photo: {
    id: 1,
    filename: "myphoto.jpg",
    status: 1,
  },
  photos: [
    {
      id: 1,
      filename: "myphoto.jpg",
      status: 1,
    },
  ],
};

export const model34 = {
  photo: {
    id: 1,
  },
};

export const model35 = {
  lastName: "Khudoiberdiev",
  photo: {
    id: 1,
  },
};

export const model36 = {
  firstName: "Umed",
  lastName: "Khudoiberdiev",
  photo: {
    id: 1,
    filename: "myphoto.jpg",
    status: 1,
  },
  photos: [
    {
      id: 1,
      filename: "myphoto.jpg",
      status: 1,
    },
  ],
};

export const model37 = {
  firstName: "Umed",
  lastName: "Khudoiberdiev",
  photo: {
    id: 1,
  },
};

export const model38 = {
  firstName: "Umed",
  lastName: "Khudoiberdiev",
  photo: {
    id: 1,
    filename: "myphoto.jpg",
  },
};

export const model39 = {
  lastName: "Khudoiberdiev",
  photo: {
    id: 1,
    status: 1,
  },
};

export const model40 = {
  lastName: "Khudoiberdiev",
  photo: {
    id: 1,
    status: 1,
  },
  photos: [
    {
      id: 1,
      status: 1,
    },
  ],
};

export const model41 = {
  firstName: "Umed",
  lastName: "Khudoiberdiev",
  name: "Umed Khudoiberdiev",
  getName: "Umed Khudoiberdiev",
};
